package models

type CreateProfessionModel struct {
	Name string `json:"name" binding:"required"`
}

type ProfessionModel struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type StatusModel struct {
	Status string `json:"status"`
}

type GetAllProfessionModel struct {
	Professions []ProfessionModel `json:"professions"`
	Count       int32             `json:"count"`
}
