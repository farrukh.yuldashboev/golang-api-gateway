package models

type CreateCompanyModel struct {
	Name string `json:"name" binding:"required"`
}

type CompanyModel struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type GetAllCompanyModel struct {
	Companies []CompanyModel `json:"companies"`
	Count     int32          `json:"count"`
}
