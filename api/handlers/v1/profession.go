package v1

import (
	"context"
	"errors"
	"net/http"

	"bitbucket.org/udevs/example_api_gateway/api/models"
	"bitbucket.org/udevs/example_api_gateway/genproto/position_service"
	"bitbucket.org/udevs/example_api_gateway/pkg/util"
	"github.com/gin-gonic/gin"
)

// Create Profession godoc
// @ID create_profession
// @Router /v1/profession [POST]
// @Summary Create Profession
// @Description Create Profession
// @Tags profession
// @Accept json
// @Produce json
// @Param profession body models.CreateProfessionModel true "profession"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreateProfession(c *gin.Context) {
	var profession models.CreateProfessionModel

	err := c.BindJSON(&profession)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}

	resp, err := h.services.ProfessionService().Create(
		context.Background(),
		&position_service.Profession{
			Name: profession.Name,
		},
	)

	if !handleError(h.log, c, err, "error while creating profession") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp.Id)
}

// Get All Profession godoc
// @ID get_all_profession
// @Router /v1/profession [GET]
// @Summary Get All Profession
// @Description Get All Profession
// @Tags profession
// @Accept json
// @Produce json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param name query string false "name"
// @Success 200 {object} models.ResponseModel{data=models.GetAllProfessionModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllProfessions(c *gin.Context) {
	var professions models.GetAllProfessionModel
	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	resp, err := h.services.ProfessionService().GetAll(
		context.Background(),
		&position_service.GetAllProfessionRequest{
			Offset: int64(offset),
			Limit:  int64(limit),
			Name:   c.Query("name"),
		},
	)

	if !handleError(h.log, c, err, "error while getting all professions") {
		return
	}
	err = ParseToStruct(&professions, resp)

	if !handleError(h.log, c, err, "error while parse to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", professions)
}

// Get Profession godoc
// @ID get_profession
// @Router /v1/profession/{profession_id} [GET]
// @Summary Get Profession
// @Description Get Profession
// @Tags profession
// @Accept json
// @Produce json
// @Param profession_id path string true "profession_id"
// @Success 200 {object} models.ResponseModel{data=models.GetAllProfessionModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetProfession(c *gin.Context) {
	var profession models.ProfessionModel
	profession_id := c.Param("profession_id")

	if !util.IsValidUUID(profession_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid profession id", errors.New("profession id is not valid"))
		return
	}

	resp, err := h.services.ProfessionService().Get(
		context.Background(),
		&position_service.ProfessionId{
			Id: profession_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting profession") {
		return
	}

	err = ParseToStruct(&profession, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", profession)
}

// Update Profession godoc
// @ID update_profession
// @Router /v1/profession/{profession_id} [PATCH]
// @Summary Update Profession
// @Description Update Profession by ID
// @Tags profession
// @Accept json
// @Produce json
// @Param profession_id path string true "profession_id"
// @Param profession body models.CreateProfessionModel true "profession"
// @Success 200 {object} models.ResponseModel{data=models.StatusModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdateProfession(c *gin.Context) {
	var status models.StatusModel
	var profession models.CreateProfessionModel

	profession_id := c.Param("profession_id")

	if !util.IsValidUUID(profession_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid profession id", errors.New("profession id is not valid"))
		return
	}

	err := c.BindJSON(&profession)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	resp, err := h.services.ProfessionService().Update(
		context.Background(),
		&position_service.Profession{
			Id:   profession_id,
			Name: profession.Name,
		},
	)

	if !handleError(h.log, c, err, "error while getting profession") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}

// Delete Profession godoc
// @ID delete_profession
// @Router /v1/profession/{profession_id} [DELETE]
// @Summary Delete Profession
// @Description Delete Profession by given ID
// @Tags profession
// @Accept json
// @Produce json
// @Param profession_id path string true "profession_id"
// @Success 200 {object} models.ResponseModel{data=models.StatusModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeleteProfession(c *gin.Context) {
	var status models.StatusModel
	profession_id := c.Param("profession_id")

	if !util.IsValidUUID(profession_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid profession id", errors.New("profession id is not valid"))
		return
	}

	resp, err := h.services.ProfessionService().Delete(
		context.Background(),
		&position_service.ProfessionId{
			Id: profession_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting profession") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}
