package v1

import (
	"context"
	"errors"
	"net/http"

	"bitbucket.org/udevs/example_api_gateway/api/models"
	"bitbucket.org/udevs/example_api_gateway/genproto/position_service"
	"bitbucket.org/udevs/example_api_gateway/pkg/util"
	"github.com/gin-gonic/gin"
)

// Create Attribute godoc
// @ID create_profession
// @Router /v1/attribute [POST]
// @Summary Create Attribute
// @Description Create Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute body models.CreateAttributeModel true "attribute"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreateAttribute(c *gin.Context) {
	var attribute models.CreateAttributeModel

	err := c.BindJSON(&attribute)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}

	resp, err := h.services.AttributeService().Create(
		context.Background(),
		&position_service.Attribute{
			Name:          attribute.Name,
			AttributeType: attribute.Type,
		},
	)

	if !handleError(h.log, c, err, "error while creating attribute") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp.Id)
}

// Get All Attribute godoc
// @ID get_all_attribute
// @Router /v1/attribute [GET]
// @Summary Get All Attribute
// @Description Get All Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param name query string false "name"
// @Success 200 {object} models.ResponseModel{data=models.GetAllAttributeModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllAttributes(c *gin.Context) {
	var attributes models.GetAllAttributeModel
	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	resp, err := h.services.AttributeService().GetAll(
		context.Background(),
		&position_service.GetAllAttributeRequest{
			Offset: int64(offset),
			Limit:  int64(limit),
			Name:   c.Query("name"),
		},
	)

	if !handleError(h.log, c, err, "error while getting all attributes") {
		return
	}
	err = ParseToStruct(&attributes, resp)

	if !handleError(h.log, c, err, "error while parse to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", attributes)
}

// Get Attribute godoc
// @ID get_attribute
// @Router /v1/attribute/{attribute_id} [GET]
// @Summary Get Attribute
// @Description Get Attribute
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Success 200 {object} models.ResponseModel{data=models.GetAllAttributeModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAttribute(c *gin.Context) {
	var attribute models.AttributeModel
	attribute_id := c.Param("attribute_id")

	if !util.IsValidUUID(attribute_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid attribute id", errors.New("attribute id is not valid"))
		return
	}

	resp, err := h.services.AttributeService().Get(
		context.Background(),
		&position_service.AttributeId{
			Id: attribute_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting attribute") {
		return
	}

	err = ParseToStruct(&attribute, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", attribute)
}

// Update Attribute godoc
// @ID update_attribue
// @Router /v1/attribute/{attribute_id} [PATCH]
// @Summary Update Attribute
// @Description Update Attribute by ID
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Param attribute body models.CreateAttributeModel true "attribute"
// @Success 200 {object} models.ResponseModel{data=models.StatusModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdateAttribute(c *gin.Context) {
	var status models.StatusModel
	var attribute models.CreateAttributeModel

	attribute_id := c.Param("attribute_id")

	if !util.IsValidUUID(attribute_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid attribute id", errors.New("attribute id is not valid"))
		return
	}

	err := c.BindJSON(&attribute)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	resp, err := h.services.AttributeService().Update(
		context.Background(),
		&position_service.Attribute{
			Id:            attribute_id,
			Name:          attribute.Name,
			AttributeType: attribute.Type,
		},
	)

	if !handleError(h.log, c, err, "error while getting attribute") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}

// Delete Attribute godoc
// @ID delete_attribute
// @Router /v1/attribute/{attribute_id} [DELETE]
// @Summary Delete Attribute
// @Description Delete Attribute by given ID
// @Tags attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Success 200 {object} models.ResponseModel{data=models.StatusModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeleteAttribute(c *gin.Context) {
	var status models.StatusModel
	attribute_id := c.Param("attribute_id")

	if !util.IsValidUUID(attribute_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid attribute id", errors.New("attribute id is not valid"))
		return
	}

	resp, err := h.services.AttributeService().Delete(
		context.Background(),
		&position_service.AttributeId{
			Id: attribute_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting attribute") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}
