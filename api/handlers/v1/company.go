package v1

import (
	"context"
	"errors"
	"net/http"

	"bitbucket.org/udevs/example_api_gateway/api/models"
	"bitbucket.org/udevs/example_api_gateway/genproto/company_service"
	"bitbucket.org/udevs/example_api_gateway/pkg/util"
	"github.com/gin-gonic/gin"
)

// Create Company godoc
// @ID create_company
// @Router /v1/company [POST]
// @Summary Create Company
// @Description Create Company
// @Tags company
// @Accept json
// @Produce json
// @Param company body models.CreateCompanyModel true "company"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreateCompany(c *gin.Context) {
	var company models.CreateCompanyModel

	err := c.BindJSON(&company)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}

	resp, err := h.services.CompanyService().Create(
		context.Background(),
		&company_service.Company{
			Name: company.Name,
		},
	)

	if !handleError(h.log, c, err, "error while creating company") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp.Id)
}

// Get All Company godoc
// @ID get_all_company
// @Router /v1/company [GET]
// @Summary Get All Company
// @Description Get All Company
// @Tags company
// @Accept json
// @Produce json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param name query string false "name"
// @Success 200 {object} models.ResponseModel{data=models.GetAllCompanyModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllCompany(c *gin.Context) {
	var companies models.GetAllCompanyModel
	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	resp, err := h.services.CompanyService().GetAll(
		context.Background(),
		&company_service.GetAllCompanyRequest{
			Offset: int64(offset),
			Limit:  int64(limit),
			Name:   c.Query("name"),
		},
	)

	if !handleError(h.log, c, err, "error while getting all companies") {
		return
	}
	err = ParseToStruct(&companies, resp)

	if !handleError(h.log, c, err, "error while parse to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", companies)
}

// Get Company godoc
// @ID get_company
// @Router /v1/company/{company_id} [GET]
// @Summary Get Company
// @Description Get Company
// @Tags company
// @Accept json
// @Produce json
// @Param company_id path string true "company_id"
// @Success 200 {object} models.ResponseModel{data=models.GetAllCompanyModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetCompany(c *gin.Context) {
	var company models.CompanyModel
	company_id := c.Param("company_id")

	if !util.IsValidUUID(company_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid company id", errors.New("company id is not valid"))
		return
	}

	resp, err := h.services.CompanyService().Get(
		context.Background(),
		&company_service.CompanyId{
			Id: company_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting company") {
		return
	}

	err = ParseToStruct(&company, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", company)
}

// Update Company godoc
// @ID update_company
// @Router /v1/company/{company_id} [PATCH]
// @Summary Update Company
// @Description Update Company by ID
// @Tags company
// @Accept json
// @Produce json
// @Param company_id path string true "company_id"
// @Param company body models.CreateCompanyModel true "company"
// @Success 200 {object} models.ResponseModel{data=models.StatusModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdateCompany(c *gin.Context) {
	var status models.StatusModel
	var company models.CreateCompanyModel

	company_id := c.Param("company_id")

	if !util.IsValidUUID(company_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid company id", errors.New("company id is not valid"))
		return
	}

	err := c.BindJSON(&company)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	resp, err := h.services.CompanyService().Update(
		context.Background(),
		&company_service.Company{
			Id:   company_id,
			Name: company.Name,
		},
	)

	if !handleError(h.log, c, err, "error while getting company") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}

// Delete Company godoc
// @ID delete_company
// @Router /v1/company/{company_id} [DELETE]
// @Summary Delete Company
// @Description Delete Company by given ID
// @Tags company
// @Accept json
// @Produce json
// @Param company_id path string true "company_id"
// @Success 200 {object} models.ResponseModel{data=models.StatusModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeleteCompany(c *gin.Context) {
	var status models.StatusModel
	company_id := c.Param("company_id")

	if !util.IsValidUUID(company_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid company id", errors.New("company id is not valid"))
		return
	}

	resp, err := h.services.CompanyService().Delete(
		context.Background(),
		&company_service.CompanyId{
			Id: company_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting company") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}
